print_string:
    push bx
    call .repeat
    pop bx

.stop:
    ret

.repeat:
    mov ah, 0x0e
    mov al, [bx]
    cmp al, 0
    je .stop
    int 0x10
    inc bx
    jmp .repeat

