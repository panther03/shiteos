#.DEFAULT_GOAL := all
#
#all: os_image.img
#
#clean:
#	rm out/*.bin
#
#run: 
#	qemu-system-i386 -fda out/os_image.img &
#
#os_image.img: boot_sect.bin kernel.bin
#	cat out/boot_sect.bin out/kernel.bin > out/os_image.img
#
#boot_sect.bin: src/boot_sect.asm
#	nasm src/boot_sect.asm -f bin -o out/boot_sect.bin
#
#kernel.bin: kernel.o kernel_entry.o
#	ld -o out/kernel.bin -Ttext 0x1000 out/kernel.o out/kernel_entry.o --oformat binary
#	rm out/*.o
#
#kernel.o: src/kernel.c
#	gcc -ffreestanding -c src/kernel.c -o out/kernel.o
#
#kernel_entry.o: src/kernel_entry.asm
#	nasm src/kernel_entry.asm -f elf64 -o out/kernel_entry.o

C_SOURCES = $(wildcard kernel/*.c drivers/*.c)
HEADERS = $(wildcard kernel/*.h drivers/*.h)

OBJ = ${C_SOURCES:.c=.o}

all: os-image.img

run:
	qemu-system-i386 -fda os-image.img &

os-image.img: boot/boot_sect.bin kernel.bin
	cat $^ > os-image.img

kernel.bin: kernel/kernel_entry.o ${OBJ}
	ld -o $@ -Ttext 0x1000 $^ --oformat binary

%.o : %.c ${HEADERS}
	gcc -ffreestanding -c $< -o $@

%.o : %.asm
	nasm $< -f elf64 -o $@

%.bin: %.asm
	nasm $< -f bin -I 'boot/' -o $@

clean:
	rm -rf *.bin *.dis *.o os-image.img
	rm -rf kernel/*.o boot/*.bin drivers/*.o
